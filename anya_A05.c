#define FUSE_USE_VERSION 28

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <wait.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>

static const char *dirPath = "/home/adindazhr/Downloads";

char animeku[10] = "Animeku_";

void enc(char *string)
{
	if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0) //current directory parent directory
		return;
	
	char filename[500]; //filename
	char ext[10]; //extension
	int filenameLength = 0, flag = 0, extLength = 0;
	memset(filename, 0, sizeof(filename));
	memset(ext, 0, sizeof(ext));
	
	for (int i = 0; i < strlen(string); i++)
	{
		if (string[i] == '/')
			continue;
		if (string[i] == '.')
		{
			filename[filenameLength++] = string[i];
			flag = 1;
		}
		if (flag == 1)
		{
			ext[extLength++] = string[i];
		}
		else
		{
			filename[filenameLength++] = string[i];
		}
	}
	
	for (int i = 0; i < filenameLength; i++)
	{
		if (isupper(filename[i])) //uppercase
			filename[i] = 'A' + 'Z' - filename[i]; //rumus atbash cipher
		else if (islower(filename[i])) //lowecase
		{
			if (filename[i] > 109)
				filename[i] -= 13; // jika n-z dikurang 13
			else
				filename[i] += 13; // jika a-m ditambah 13
		}
	}

	strcat(filename, ext); 
	strcpy(string, filename);
}

void logged(char *command, char *old, char *new)
{
	time_t times;
	struct tm *timeinfo;
	time(&times);
	timeinfo = localtime(&times);
	
	FILE *fptr;
	fptr = fopen("/home/adindazhr/Wibu.log", "a");
	if (fptr == NULL)
	{
		printf("[Error] : [Gagal dalam membuka file]");
    	exit(1);
	}
	else
	{
    	if (strcmp(command, "RENAME") == 0)
    		fprintf(fptr, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, 				timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
    	else
    		fprintf(fptr, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
	}
	fclose(fptr);
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
	char *str = strstr(path, animeku);
	if (str != NULL)
	{
		str += strlen(animeku);
		char *temp = strchr(str, '/');
		if (temp != NULL)
		{
			temp += 1;
			enc(temp);
		}
	}
	
	int res;
	char fpath[1000];
	
	if (strcmp(path, "/") == 0)
	{
		path = dirPath;
		sprintf(fpath, "%s", path);
	}
	else
	{
		sprintf(fpath, "%s%s", dirPath, path);
	}
	
	res = lstat(fpath, stbuf);
	if (res == -1)
		return -errno;
	
	return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
	char *str = strstr(path, animeku);
	if (str != NULL)
	{
		str += strlen(animeku);
		char *temp = strchr(str, '/');
		if (temp != NULL)
		{
			temp += 1;
			enc(temp);
		}
	}
	
	int res;
	char fpath[1000];

	if (strcmp(path, "/") == 0)
	{
		path = dirPath;
		sprintf(fpath, "%s", path);
	}
	else
	{
		sprintf(fpath, "%s%s", dirPath, path);
	}
	
	DIR *dp;
	struct dirent *de;
	(void)offset;
	(void)fi;
	dp = opendir(fpath);
	
	if (dp == NULL)
		return -errno;

	while ((de = readdir(dp)) != NULL)
	{
    	if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
        	continue;
    	struct stat st;
    	memset(&st, 0, sizeof(st));
    	st.st_ino = de->d_ino;
    	st.st_mode = de->d_type << 12;

    	if (str != NULL)
        	enc(de->d_name);

    	res = (filler(buf, de->d_name, &st, 0));
    	if (res != 0)
        	break;
  	}

	logged("READDIR", fpath, "");

	closedir(dp);
	return 0;
}

static int xmp_rename(const char *source, const char *destination)
{
	int res;
	char fpath_source[1000];
	char fpath_destination[1000];
	// char *p_fpath_source, *p_fpath_destination;

	if (strcmp(source, "/") == 0)
	{
    	source = dirPath;
    	sprintf(fpath_source, "%s", source);
    }
	else
    	sprintf(fpath_source, "%s%s", dirPath, source);

	if (strcmp(source, "/") == 0)
    	sprintf(fpath_destination, "%s", dirPath);
	else
    	sprintf(fpath_destination, "%s%s", dirPath, destination);

	res = rename(fpath_source, fpath_destination);
	if (res == -1)
    	return -errno;
    	
    	logged("RENAME", fpath_source, fpath_destination);

	// p_fpath_source = strrchr(fpath_source, '/');
	// p_fpath_destination = strrchr(fpath_destination, '/');

  return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	if (a != NULL) enc(a);

	if (strcmp(path, "/") == 0){path = dirPath; sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	int result = 0;
	int fd = 0;

	(void)fi;

	fd = open(filepath, O_RDONLY);
	if (fd == -1) return -errno;

	result = pread(fd, buf, size, offset);
	if (result == -1) result = -errno;
	
	logged("READ", filepath, "");


	close(fd);
	return result;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr, //mengembalikan attribute dari suatu file
    .readdir = xmp_readdir, //pembacaan directory
    .read = xmp_read, //pembacaan file
    .rename = xmp_rename //rename directory maupun file
};

int main(int argc, char *argv[])
{
  umask(0);
  return fuse_main(argc, argv, &xmp_oper, NULL);
}
