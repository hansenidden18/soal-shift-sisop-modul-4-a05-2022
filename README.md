# soal-shift-sisop-modul-4-A05-2022

## Nomor 1

### Deskripsi Soal

Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:

#### 1.A

Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
Contoh :
“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”

##### Membuat fungsi encode
Variabel filename akan berisi nama file dan variabel ext akan berisi extension dari file tersebut. Selanjutnya, filename akan dicek apakah filename tersebut uppercase atau lowercase. Jika uppercase maka akan diencode dengan atbash cipher dan jika lowercase maka akan diencode dengan rot13.
```
void enc(char *string)
{
	if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0) //current directory parent directory
		return;
	
	char filename[500]; //filename
	char ext[10]; //extension
	int filenameLength = 0, flag = 0, extLength = 0;
	memset(filename, 0, sizeof(filename));
	memset(ext, 0, sizeof(ext));
	
	for (int i = 0; i < strlen(string); i++)
	{
		if (string[i] == '/')
			continue;
		if (string[i] == '.')
		{
			filename[filenameLength++] = string[i];
			flag = 1;
		}
		if (flag == 1)
		{
			ext[extLength++] = string[i];
		}
		else
		{
			filename[filenameLength++] = string[i];
		}
	}
	
	for (int i = 0; i < filenameLength; i++)
	{
		if (isupper(filename[i])) //uppercase
			filename[i] = 'A' + 'Z' - filename[i]; //rumus atbash cipher
		else if (islower(filename[i])) //lowecase
		{
			if (filename[i] > 109)
				filename[i] -= 13; // jika n-z dikurang 13
			else
				filename[i] += 13; // jika a-m ditambah 13
		}
	}

	strcat(filename, ext); 
	strcpy(string, filename);
}
```
##### Struktur Fuse
Struktur fuse yang kami gunakan adalah sebagai berikut
```
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr, //mengembalikan attribute dari suatu file
    .readdir = xmp_readdir, //pembacaan directory
    .read = xmp_read, //pembacaan file
    .rename = xmp_rename //rename directory maupun file
};
```
* getattr digunakan untuk mengembalikan atribut dari suatu file
* readdir digunakan untuk membaca suatu directory
* read digunakan untuk membaca suatu file
* rename digunakan untuk rename suatu directory atau file

##### Fungsi readdir
Fungsi ini akan digunakan untuk membaca suatu directory. Sehingga, directory tersebut dan file dan directory dalam suatu directory dapat diencode.
```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
	char *str = strstr(path, animeku);
	if (str != NULL)
	{
		str += strlen(animeku);
		char *temp = strchr(str, '/');
		if (temp != NULL)
		{
			temp += 1;
			enc(temp);
		}
	}
	
	int res;
	char fpath[1000];

	if (strcmp(path, "/") == 0)
	{
		path = dirPath;
		sprintf(fpath, "%s", path);
	}
	else
	{
		sprintf(fpath, "%s%s", dirPath, path);
	}
	
	DIR *dp;
	struct dirent *de;
	(void)offset;
	(void)fi;
	dp = opendir(fpath);
	
	if (dp == NULL)
		return -errno;

	while ((de = readdir(dp)) != NULL)
	{
    	if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
        	continue;
    	struct stat st;
    	memset(&st, 0, sizeof(st));
    	st.st_ino = de->d_ino;
    	st.st_mode = de->d_type << 12;

    	if (str != NULL)
        	enc(de->d_name);

    	res = (filler(buf, de->d_name, &st, 0));
    	if (res != 0)
        	break;
  	}

	logged("READDIR", fpath, "");

	closedir(dp);
	return 0;
}
```
#### 1.B

Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

##### Fungsi rename

Fungsi ini akan digunakan untuk merename suatu directory atau file. Setiap direname, directory atau file tersebut akan dicek apakah ada Animeku_ jika ada maka akan di encode.
```
static int xmp_rename(const char *source, const char *destination)
{
	int res;
	char fpath_source[1000];
	char fpath_destination[1000];
	// char *p_fpath_source, *p_fpath_destination;

	if (strcmp(source, "/") == 0)
	{
    	source = dirPath;
    	sprintf(fpath_source, "%s", source);
    }
	else
    	sprintf(fpath_source, "%s%s", dirPath, source);

	if (strcmp(source, "/") == 0)
    	sprintf(fpath_destination, "%s", dirPath);
	else
    	sprintf(fpath_destination, "%s%s", dirPath, destination);

	res = rename(fpath_source, fpath_destination);
	if (res == -1)
    	return -errno;
    	
    	logged("RENAME", fpath_source, fpath_destination);

	// p_fpath_source = strrchr(fpath_source, '/');
	// p_fpath_destination = strrchr(fpath_destination, '/');

  return 0;
}
```

#### 1.C

Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

##### Decode
Dikarenakan rumus encode dan decode sama maka untuk decode akan menggunakan fungsi yang sama dengan encode.

#### 1.D

Setiap data yang terencode akan masuk dalam file “Wibu.log”
Contoh isi:
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba

##### Fungsi Log

Fungsi ini digunakan untuk membuat file log

```
void logged(char *command, char *old, char *new)
{
	time_t times;
	struct tm *timeinfo;
	time(&times);
	timeinfo = localtime(&times);
	
	FILE *fptr;
	fptr = fopen("/home/adindazhr/Wibu.log", "a");
	if (fptr == NULL)
	{
		printf("[Error] : [Gagal dalam membuka file]");
    	exit(1);
	}
	else
	{
    	if (strcmp(command, "RENAME") == 0)
    		fprintf(fptr, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, 				timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
    	else
    		fprintf(fptr, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
	}
	fclose(fptr);
}
```

#### 1.E
Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

Masih belum bisa dilakukan

#### Sebelum dirun

![image.png](./Dokumentasi/image.png)

![image-1.png](./Dokumentasi/image-1.png)

#### Setelah dirun

![image-2.png](./Dokumentasi/image-2.png)

![image-3.png](./Dokumentasi/image-3.png)

![image-4.png](./Dokumentasi/image-4.png)

![image-5.png](./Dokumentasi/image-5.png)

![image-6.png](./Dokumentasi/image-6.png)

#### Kendala

* Saat dirun, directory berhasil terencode tapi file tidak terbaca. Sehingga file terkesan hilang, namun jika didecode kembali, file muncul kembali.
* Encode masih belum bisa dilakukan secara rekursif.


## Nomor 2 dan 3

#### Kendala

* Dikarenakan nomor 1 belum bisa diselesaikan dengan benar, maka otomatis nomor 2 dan 3 tidak bisa dikerjakan.
